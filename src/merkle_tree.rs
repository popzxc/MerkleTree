use ring::digest::Algorithm;
use tree::Tree;
use hash::{HashUtils, Hashable};
use std::collections::LinkedList;

/// Structure that represents Merkle Tree
pub struct MerkleTree<T: Hashable> {
    /// Hashing algorithm used for the tree
    pub algorithm: &'static Algorithm,
    /// Root of the tree
    root: Tree<T>,
    /// Height of the tree
    height: usize,
    /// Count of leaves
    count: usize,
}

impl<T: Hashable> MerkleTree<T> {
    /// Construct merkle tree from the vector of values
    pub fn from_vec(algorithm: &'static Algorithm, values: Vec<T>) -> Result<Self, &str> {
        if values.is_empty() {
            return Result::Err("Expected vec to be not empty");
        }

        let count = values.len();
        let mut height = 0;
        // Layers are stored as linked lists due to their O(1) append and pop_front operations
        let mut current_layer = LinkedList::new();

        // Create leaves as the lowest layer of the tree
        for v in values {
            let leaf = Tree::new_leaf(algorithm, v);
            current_layer.push_back(leaf);
        }

        // Fill the tree until the root is achieved
        while current_layer.len() > 1 {
            let mut next_layer = LinkedList::new();

            while !current_layer.is_empty() {
                // If the number of nodes is odd, we're creating additional node
                // This node copies the hash of the previous node
                if current_layer.len() == 1 {
                    let filling_node = Tree::new_filling_node(current_layer.front().unwrap());
                    current_layer.push_back(filling_node);
                }

                // Hash for the node is calculated from the hashes of its children
                let left = current_layer.pop_front().unwrap();
                let right = current_layer.pop_front().unwrap();

                let combined_hash = algorithm.hash_node(left.hash(), right.hash());

                let node = Tree::Node {
                    hash: combined_hash.as_ref().into(),
                    left: Box::new(left),
                    right: Box::new(right),
                };

                next_layer.push_back(node);
            }

            height += 1;

            current_layer = next_layer;
        }

        debug_assert!(current_layer.len() == 1);

        let root = current_layer.pop_front().unwrap();

        Result::Ok(MerkleTree {
            algorithm: algorithm,
            root: root,
            height: height,
            count: count,
        })
    }

    /// Get the hash value of the root (also known as merkle root)
    pub fn root_hash(&self) -> &Vec<u8> {
        self.root.hash()
    }

    /// Get the height of the tree
    pub fn height(&self) -> usize {
        self.height
    }

    /// Get the amount of leaves in the tree
    pub fn count(&self) -> usize {
        self.count
    }

    /// Get the root node of the tree
    pub fn root(&self) -> &Tree<T> {
        &self.root
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ring::digest::SHA512;
    use ring::digest::Digest;

    #[allow(non_upper_case_globals)]
    static digest: &'static Algorithm = &SHA512;

    #[test]
    fn tree_empty_test() {
        let values: Vec<&str> = vec![];

        let tree_result = MerkleTree::from_vec(digest, values);

        assert!(tree_result.is_err());
    }

    #[test]
    fn tree_not_empty_test() {
        let values: Vec<&str> = vec!["one"];

        let tree_result = MerkleTree::from_vec(digest, values);

        assert!(tree_result.is_ok());
    }

    #[test]
    fn tree_from_vec_test() {
        let values = vec!["one", "two", "three", "four"];

        let hashes: Vec<Digest> = values
            .iter()
            .map(|a| digest.hash_leaf(&a.as_bytes()))
            .collect();

        let count = values.len();
        let tree = MerkleTree::from_vec(digest, values).unwrap();

        let h01 = digest.hash_node(&hashes[0], &hashes[1]);
        let h23 = digest.hash_node(&hashes[2], &hashes[3]);

        let root_hash = digest.hash_node(&h01, &h23);

        assert_eq!(tree.count(), count);
        assert_eq!(tree.height(), 2);
        assert_eq!(tree.root_hash().as_slice(), root_hash.as_ref());
    }

    #[test]
    fn tree_with_odd_leafs_test() {
        let values = vec!["one", "two", "three"];

        let hashes: Vec<Digest> = values
            .iter()
            .map(|a| digest.hash_leaf(&a.as_bytes()))
            .collect();

        let count = values.len();
        let tree = MerkleTree::from_vec(digest, values).unwrap();

        let h01 = digest.hash_node(&hashes[0], &hashes[1]);
        let h23 = digest.hash_node(&hashes[2], &hashes[2]);

        let root_hash = digest.hash_node(&h01, &h23);

        assert_eq!(tree.count(), count);
        assert_eq!(tree.height(), 2);
        assert_eq!(tree.root_hash().as_slice(), root_hash.as_ref());
    }
}
