extern crate ring;

/// This module implements hashing interface for the Merkle Tree
pub mod hash;
/// This module contains the Merkle Tree itself
pub mod merkle_tree;
/// This module contains functions for generating audit proof and applying it to the element
pub mod audit_proof;
mod tree;
