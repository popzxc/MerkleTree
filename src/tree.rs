use ring::digest::{Algorithm, Digest};

use hash::{HashUtils, Hashable};

/// Binary Tree where leaves hold a stand-alone value.
pub enum Tree<T: Hashable> {
    Leaf {
        hash: Vec<u8>,
        value: T,
    },

    FillingNode {
        hash: Vec<u8>,
    },

    Node {
        hash: Vec<u8>,
        left: Box<Tree<T>>,
        right: Box<Tree<T>>,
    },
}

impl<T: Hashable> Tree<T> {
    /// Create a new tree
    pub fn new(hash: Digest, value: T) -> Self {
        Tree::Leaf {
            hash: hash.as_ref().into(),
            value: value,
        }
    }

    /// Create a new leaf
    pub fn new_leaf(algo: &'static Algorithm, value: T) -> Tree<T> {
        let hash = algo.hash_leaf(&value);
        Tree::new(hash, value)
    }

    /// Create a filling node that copies hash of another one
    pub fn new_filling_node(node: &Tree<T>) -> Tree<T> {
        Tree::FillingNode {
            hash: node.hash().clone(),
        }
    }

    /// Returns a hash from the tree.
    pub fn hash(&self) -> &Vec<u8> {
        match *self {
            Tree::Leaf { ref hash, .. }
            | Tree::FillingNode { ref hash, .. }
            | Tree::Node { ref hash, .. } => hash,
        }
    }
}
