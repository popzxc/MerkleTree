use ring::digest::{Algorithm, Context, Digest};

/// A trait for elements of tree to be compatible with ring library hashig interfaces
pub trait Hashable {
    /// Function for adding element data to the ring hashing context
    fn update_context(&self, context: &mut Context);
}

/// Every object that can be represented as a sequence of bytes is hashable trivially
impl<T: AsRef<[u8]>> Hashable for T {
    fn update_context(&self, context: &mut Context) {
        context.update(self.as_ref());
    }
}

/// A trait for calculating hash values for both leaves and nodes of the tree
///
/// Result of hash functions is the ring's Digest object
pub trait HashUtils {
    /// Calculates a hash for the leaf object
    fn hash_leaf<T: Hashable>(&'static self, leaf: &T) -> Digest;
    /// Calculates a hash for the node object
    fn hash_node<T: Hashable>(&'static self, left: &T, right: &T) -> Digest;
}

/// A trait for ring algorithms to be able to hash both leaves and nodes of tree
impl HashUtils for Algorithm {
    /// Hashing of the leaf calclates simple hash(leaf_value)
    fn hash_leaf<T: Hashable>(&'static self, leaf: &T) -> Digest {
        let mut ctx = Context::new(self);
        leaf.update_context(&mut ctx);
        ctx.finish()
    }
    /// Hashing of node performs hash(concat(hash_left, hash_right))
    fn hash_node<T: Hashable>(&'static self, left: &T, right: &T) -> Digest {
        let mut ctx = Context::new(self);
        left.update_context(&mut ctx);
        right.update_context(&mut ctx);
        ctx.finish()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ring::digest::SHA512;

    #[allow(non_upper_case_globals)]
    static digest: &'static Algorithm = &SHA512;

    #[test]
    fn leaf_test() {
        let test_val: Vec<u8> = vec![1, 2, 3];
        let gotten_result = digest.hash_leaf(&test_val);

        let mut ctx = Context::new(digest);
        test_val.update_context(&mut ctx);
        let expected_result = ctx.finish();

        assert_eq!(gotten_result.as_ref(), expected_result.as_ref());
    }

    #[test]
    fn nodes_test() {
        let test_val_a: Vec<u8> = vec![1, 2, 3];
        let test_val_b: Vec<u8> = vec![1, 2, 3];
        let gotten_result = digest.hash_node(&test_val_a, &test_val_b);

        let mut ctx = Context::new(digest);
        test_val_a.update_context(&mut ctx);
        test_val_b.update_context(&mut ctx);
        let expected_result = ctx.finish();

        assert_eq!(gotten_result.as_ref(), expected_result.as_ref());
    }
}
