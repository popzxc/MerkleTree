use audit_proof::proof_element::ProofElement;

/// Enum that contains a sequence of pairs (hash, direction)
#[derive(PartialEq, Eq, Debug)]
pub enum Proof {
    DoNotExist,
    Exist { proof: Vec<ProofElement> },
}

impl Proof {
    pub fn append(&mut self, proof_element: ProofElement) {
        match *self {
            Proof::DoNotExist => {}
            Proof::Exist { ref mut proof } => {
                proof.push(proof_element);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn append_test() {
        let first = ProofElement::create_left(&vec![]);
        let second = ProofElement::create_left(&vec![]);

        let mut proof_obj = Proof::Exist { proof: vec![first] };
        match proof_obj {
            Proof::Exist { ref proof } => assert_eq!(proof.len(), 1),
            _ => {}
        }
        proof_obj.append(second);
        match proof_obj {
            Proof::Exist { ref proof } => assert_eq!(proof.len(), 2),
            _ => {}
        }
        assert_eq!(
            proof_obj,
            Proof::Exist {
                proof: vec![
                    ProofElement::create_left(&vec![]),
                    ProofElement::create_left(&vec![]),
                ],
            }
        );
    }
}
