use ring::digest::Algorithm;
use hash::HashUtils;

#[derive(PartialEq, Eq, Debug)]
pub enum ProofElement {
    Right { hash: Vec<u8> },
    Left { hash: Vec<u8> },
}

impl ProofElement {
    pub fn create_left(hash: &[u8]) -> Self {
        ProofElement::Left {
            hash: hash.to_owned(),
        }
    }

    pub fn create_right(hash: &[u8]) -> Self {
        ProofElement::Right {
            hash: hash.to_owned(),
        }
    }

    /// Calculates a new hash for previous one an the ProofElement depending on its direction
    pub fn calc_hash(&self, other: &[u8], algorithm: &'static Algorithm) -> Vec<u8> {
        match *self {
            ProofElement::Left { ref hash } => {
                algorithm.hash_node(&hash.as_ref(), &other).as_ref().into()
            }
            ProofElement::Right { ref hash } => {
                algorithm.hash_node(&other, &hash.as_ref()).as_ref().into()
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ring::digest::{Algorithm, SHA512};

    #[allow(non_upper_case_globals)]
    static digest: &'static Algorithm = &SHA512;

    #[test]
    fn left_creates_test() {
        let left = ProofElement::create_left(&vec![]);
        assert_eq!(left, ProofElement::Left { hash: vec![] });
    }

    #[test]
    fn right_creates_test() {
        let right = ProofElement::create_right(&vec![]);
        assert_eq!(right, ProofElement::Right { hash: vec![] });
    }

    #[test]
    fn calc_hash_left_test() {
        let hash_left: Vec<u8> = vec![1, 2];
        let hash_right: Vec<u8> = vec![3, 4];

        let left = ProofElement::create_left(&hash_left);

        let calculated_hash = left.calc_hash(hash_right.as_ref(), digest);

        let expected_result = digest.hash_node(&hash_left, &hash_right);
        let not_expected_result = digest.hash_node(&hash_right, &hash_left);

        assert_eq!(calculated_hash, expected_result.as_ref());
        assert_ne!(calculated_hash, not_expected_result.as_ref());
    }

    #[test]
    fn calc_hash_right_test() {
        let hash_left: Vec<u8> = vec![1, 2];
        let hash_right: Vec<u8> = vec![3, 4];

        let right = ProofElement::create_right(&hash_right);

        let calculated_hash = right.calc_hash(hash_left.as_ref(), digest);

        let expected_result = digest.hash_node(&hash_left, &hash_right);
        let not_expected_result = digest.hash_node(&hash_right, &hash_left);

        assert_eq!(calculated_hash, expected_result.as_ref());
        assert_ne!(calculated_hash, not_expected_result.as_ref());
    }
}
