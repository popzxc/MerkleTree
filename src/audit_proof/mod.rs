mod proof;
mod proof_element;

use audit_proof::proof::*;
use audit_proof::proof_element::*;
use ring::digest::Algorithm;
use hash::{HashUtils, Hashable};
use merkle_tree::MerkleTree;
use tree::Tree;

#[derive(Debug, PartialEq, Eq)]
pub enum ProofResult {
    ElementNotFound,
    IncorrectProof,
    CorrectProof,
}

fn get_proof_from_node<T: Hashable + Eq>(element: &T, left: &Tree<T>, right: &Tree<T>) -> Proof {
    // Firstly, we try to find element in the left subtree
    // And then we go into the right subtree only element wasn't found in the left one
    let mut left_proof = lookup_proof(element, left);
    let mut right_proof = match left_proof {
        Proof::DoNotExist => lookup_proof(element, right),
        _ => Proof::DoNotExist,
    };

    if (left_proof != Proof::DoNotExist) && (right_proof == Proof::DoNotExist) {
        left_proof.append(ProofElement::create_right(right.hash()));
        left_proof
    } else if (left_proof == Proof::DoNotExist) && (right_proof != Proof::DoNotExist) {
        right_proof.append(ProofElement::create_left(left.hash()));
        right_proof
    } else {
        Proof::DoNotExist
    }
}

fn get_proof_from_leaf<T: Hashable + Eq>(element: &T, value: &T) -> Proof {
    if value == element {
        Proof::Exist { proof: vec![] }
    } else {
        Proof::DoNotExist
    }
}

fn lookup_proof<T: Hashable + Eq>(element: &T, tree_root: &Tree<T>) -> Proof {
    match *tree_root {
        Tree::Node {
            ref left,
            ref right,
            ..
        } => get_proof_from_node(element, left, right),
        Tree::Leaf { ref value, .. } => get_proof_from_leaf(element, value),
        Tree::FillingNode { .. } => Proof::DoNotExist,
    }
}

/// Builds an audit proof with which you can determine
/// if a certain element persists in the tree or not
///
/// Proof is a chain of hashes such that folding it with a hash function
/// (with an element hash as accumulator) will give you a Merkle Root
///
/// # Arguments
/// * `element` - element which you want to check for being in the tree
/// * `tree` - a Merkle Tree that is checked for containing the element
///
/// # Return value
/// * A proof objects
pub fn audit_proof<T: Hashable + Eq>(element: &T, tree: &MerkleTree<T>) -> Proof {
    lookup_proof(element, tree.root())
}

/// Checks if element exists in tree based on the element itself, a proof,
/// and a Merkle root hash
///
/// # Arguments
/// * `element` - element which you want to check for being in the tree
/// * `proof` - a proof object that contains hashes for getting a Merkle root
/// * `root_hash` - Merkle root hash which we should obtain after applying a proof
///
/// # Return value
/// * `true` if element is in the tree
/// * `false` otherwise
pub fn apply_proof<T: Hashable>(
    element: &T,
    proof: &Proof,
    root_hash: &[u8],
    algorithm: &'static Algorithm,
) -> ProofResult {
    match *proof {
        Proof::DoNotExist => ProofResult::ElementNotFound,
        Proof::Exist { ref proof } => {
            if proof.is_empty() {
                return ProofResult::ElementNotFound;
            }
            let start_hash: Vec<u8> = algorithm.hash_leaf(element).as_ref().into();
            let final_hash = proof.iter().fold(start_hash, |folding_hash, proof_el| {
                proof_el.calc_hash(&folding_hash, algorithm)
            });
            if final_hash == root_hash {
                ProofResult::CorrectProof
            } else {
                ProofResult::IncorrectProof
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ring::digest::{Algorithm, SHA512};

    #[allow(non_upper_case_globals)]
    static digest: &'static Algorithm = &SHA512;

    #[test]
    fn proof_not_exist_test() {
        let values = vec!["one", "two"];
        let tree = MerkleTree::from_vec(digest, values).unwrap();
        let proof = audit_proof(&"three", &tree);

        assert_eq!(proof, Proof::DoNotExist);

        assert_eq!(
            apply_proof(&"three", &proof, tree.root_hash(), digest),
            ProofResult::ElementNotFound
        );
    }

    #[test]
    fn proof_exist_test() {
        let values = vec!["one", "two", "three", "four", "five", "six", "seven"];
        let tree = MerkleTree::from_vec(digest, values).unwrap();
        let proof = audit_proof(&"two", &tree);

        assert_ne!(proof, Proof::DoNotExist);

        assert_eq!(
            apply_proof(&"two", &proof, tree.root_hash(), digest),
            ProofResult::CorrectProof
        );
        assert_eq!(
            apply_proof(&"aaa", &proof, tree.root_hash(), digest),
            ProofResult::IncorrectProof
        );
    }
}
