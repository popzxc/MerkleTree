#![feature(test)]
extern crate merkle_tree;
extern crate ring;
extern crate test;

use merkle_tree::audit_proof::audit_proof;
use merkle_tree::merkle_tree::MerkleTree;
use ring::digest::{Algorithm, SHA512};
use test::{black_box, Bencher};

#[allow(non_upper_case_globals)]
static digest: &'static Algorithm = &SHA512;

#[bench]
fn bench_create_merkle_tree(b: &mut Bencher) {
    let values: Vec<String> = (0..1000).map(|a| a.to_string()).collect();

    // This bench is not really accurate as it also consumes costs for vector cloning
    // But at least it can detect if creating of the tree become too slow
    b.iter(|| black_box(MerkleTree::from_vec(digest, values.clone())));
}

#[bench]
fn bench_audit_proof_left_half(b: &mut Bencher) {
    let values: Vec<String> = (0..1000).map(|a| a.to_string()).collect();
    let tree = MerkleTree::from_vec(digest, values).unwrap();
    let elem = test::black_box(String::from("0"));
    b.iter(|| black_box(audit_proof(&elem, &tree)));
}

#[bench]
fn bench_audit_proof_center(b: &mut Bencher) {
    let values: Vec<String> = (0..1000).map(|a| a.to_string()).collect();
    let tree = MerkleTree::from_vec(digest, values).unwrap();
    let elem = black_box(String::from("500"));
    b.iter(|| black_box(audit_proof(&elem, &tree)));
}

#[bench]
fn bench_audit_proof_right_half(b: &mut Bencher) {
    let values: Vec<String> = (0..1000).map(|a| a.to_string()).collect();
    let tree = MerkleTree::from_vec(digest, values).unwrap();
    let elem = black_box(String::from("1000"));
    b.iter(|| black_box(audit_proof(&elem, &tree)));
}
