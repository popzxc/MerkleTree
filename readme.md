# MerkleTree

Minimalistic implementation of Merkle Tree in rust

Supported features:
- Creating of immutable Merkle Tree with access to the root hash
- Choosing appropriate hashing algorithm based on the `ring` library
- Finding an audit proof and applying it

# Sample usage example
```rust
static digest: &'static Algorithm = &SHA512;

let values = vec!["one", "two", "three", "four", "five", "six", "seven"];
let tree = MerkleTree::from_vec(digest, values).unwrap();
let proof = audit_proof(&"two", &tree);
let element_exists_in_tree = apply_proof(&"two", &proof, tree.root_hash(), digest);
if element_exists_in_tree {
    println!("Exists!");
} else {
    println!("Does not exist!");
}
```

# Developers notes
Developers notes are available [here](developers_notes.md) (in russian)